FROM node:20-bookworm-slim

RUN npm i -g pnpm

COPY package.json /app/package.json
COPY pnpm-lock.yaml /app/pnpm-lock.yaml

WORKDIR /app

RUN pnpm i

COPY . /app/

CMD ["pnpm", "start"]